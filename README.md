README - Triple Store (Triple Store Plugin)
=======================================

Plugin to generate RDF triples.

Plugin Dependencies
-------------------

sudo apt-get install python-psycopg2 python-pip
sudo pip install rdflib

Enable Plugin
-------------

Also, you need to enable Triple Store Plugin at your Noosfero:

cd <your_noosfero_dir>
./script/noosfero-plugins enable triple_store

Activate Plugin
-------------

As a Noosfero administrator user, go to administrator panel:

- Click on "Plugins" option
- Click on "Triple Store Plugin" check-box

Configure Plugin
----------------

As a Noosfero administrator user, go to administrator panel:

- Click on "Configuration" below the "Triple Store Plugin"
- Define how often the plugin will run on "Period (hours) for generating triples
  (default is 24)"
- Save your changes

The "Enable" button will be displayed if the plugin is not scheduled. Click on
it to enable it.
The "Disable" button will be displayed if the plugin is already scheduled. Click on
it to disable it.
