class TripleStorePluginAdminController < AdminController
  append_view_path File.join(File.dirname(__FILE__) + '/../views')

  def index
    @settings ||= Noosfero::Plugin::Settings.new(environment, TripleStorePlugin, params[:settings])
    if scheduled_scan_exists?(settings.scheduled_scan)
      @next_run = Delayed::Job.find(settings.scheduled_scan).run_at.localtime
    end
    if request.post?
      settings.period = settings.period.blank? ? TripleStorePlugin.default_period : settings.period.to_f
      settings.save!
      redirect_to :action => 'index'
    end
  end

  def enable
    settings.enabled = true
    if settings.save!
      TripleStorePlugin.schedule_scan(environment)
    end
    redirect_to :action => 'index'
  end

  def disable
    settings.enabled = false
    settings.save!
    remove_scheduled_scan
    redirect_to :action => 'index'
  end

  private

  def settings
    @settings ||= Noosfero::Plugin::Settings.new(environment, TripleStorePlugin)
  end

  def remove_scheduled_scan
    if scheduled_scan_exists?(settings.scheduled_scan) && Delayed::Job.find(settings.scheduled_scan).destroy
      settings.scheduled_scan = nil
      settings.save!
    end
  end

  def scheduled_scan_exists?(job_id)
    settings.scheduled_scan && Delayed::Job.find_by_id(settings.scheduled_scan)
  end
end

