class TripleStorePlugin < Noosfero::Plugin

  def self.plugin_name
    "Triple Store Plugin"
  end

  def self.plugin_description
    _("Cria triplas RDF.")
  end

  # defined in hours
  def self.default_period
    24
  end

  def self.schedule_scan(environment)
    settings = Noosfero::Plugin::Settings.new(environment, self)
    if settings.enabled
      db_file = Rails.root.join('config', 'database.yml')
      db_config = YAML.load_file(db_file)
      db_config.each do |env, attr|
        next unless env.match(/#{Rails.env}$/)
        time = DateTime.now + settings.period.to_f.hour
        job = Delayed::Job.enqueue(TripleStorePlugin::TriplesJob.new(environment.id, attr['database'], attr['username']), :run_at => time)
        settings.scheduled_scan = job.id
        settings.save!
        Rails.logger.info("\n\n==> #{self.plugin_name} scheduled for #{env} environment: #{time}.\n\n") if Rails.logger
      end
    else
      Rails.logger.warn("\n\n==> #{self.plugin_name} is disabled, it's not possible to schedule scan.\n\n") if Rails.logger
    end
  end

end
