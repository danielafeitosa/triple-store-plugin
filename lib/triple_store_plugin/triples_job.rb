class TripleStorePlugin::TriplesJob < Struct.new(:environment_id, :database_name, :database_user)
  def perform
    environment = Environment.find(environment_id)
    settings = Noosfero::Plugin::Settings.new(environment, TripleStorePlugin)
    opa_file = File.join(TripleStorePlugin.root_path, 'files', 'opa.owl')
    output_folder = File.join(TripleStorePlugin.root_path, 'public')
    fork {system("python #{File.join(TripleStorePlugin.root_path, 'files', 'triplificaParticipa2.py')} #{database_name} #{database_user} #{opa_file} #{output_folder}") }
    TripleStorePlugin.schedule_scan(environment)
  end
end
